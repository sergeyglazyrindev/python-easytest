from copy import deepcopy
from unittest import TestCase
from easytest.openapi.data_generators import (
    DataGenerator,
    BaseCustomDataGenerator
)
from .common import Request


parameters_skeleton = {
    "in": "body",
    "name": "data",
    "schema": {
        "properties": {
        },
        "type": "object"
    }
}

generator = DataGenerator()


class CustomizedGeneratorForFieldEmail(BaseCustomDataGenerator):

    @classmethod
    def generate_if_needed_data_based_on_context(cls, field, request_context):
        if field.name == 'email':
            return 'example@example.com'

generator.register_specific_generator(CustomizedGeneratorForFieldEmail)


class DataGeneratorTest(TestCase):

    def clone_schema_skeleton(self, extras_for_parameters):
        schema = deepcopy(parameters_skeleton)
        schema['schema']['properties'].update(extras_for_parameters)
        return [schema]

    def test_integer(self):
        schema = self.clone_schema_skeleton({
            "id": {
                "description": "",
                "type": "integer"
            }
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(type(generated['id']), int)

    def test_string(self):
        schema = self.clone_schema_skeleton({
            "avatar": {
                "description": "",
                "type": "string"
            },
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(type(generated['avatar']), str)

    def test_string_with_choices(self):
        schema = self.clone_schema_skeleton({
            "came_from": {
                "description": "",
                "type": "string",
                "choices": ["default"]
            },
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(generated['came_from'], 'default')

    def test_integer_array(self):
        schema = self.clone_schema_skeleton({
            "ids": {
                "description": "",
                "items": {
                    "type": "integer"
                },
                "type": "array"
            },
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(type(generated['ids']), list)
        for id in generated['ids']:
            self.assertEquals(type(id), int)

    def test_string_array(self):
        schema = self.clone_schema_skeleton({
            "ids": {
                "description": "",
                "items": {
                    "type": "string"
                },
                "type": "array"
            },
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(type(generated['ids']), list)
        for id in generated['ids']:
            self.assertEquals(type(id), str)

    def test_array_with_choices(self):
        schema = self.clone_schema_skeleton({
            "ids": {
                "description": "",
                "items": {
                    "type": "string"
                },
                "type": "array",
                "choices": ["1", "2"]
            },
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(type(generated['ids']), list)
        for id in generated['ids']:
            self.assertTrue(id in ["1", "2"])

    def test_default(self):
        schema = self.clone_schema_skeleton({
            "ids": {
                "description": "",
                "default": "UAHH"
            },
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(generated['ids'], 'UAHH')

    def test_boolean(self):
        schema = self.clone_schema_skeleton({
            "id": {
                "description": "",
                "type": "boolean"
            }
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(type(generated['id']), bool)

    def test_custom_generator_for_email(self):
        schema = self.clone_schema_skeleton({
            "email": {
                "description": "",
                "type": "string"
            }
        })
        generated = generator.generate_fake_request_data(
            schema,
            Request()
        )
        self.assertEquals(generated['email'], 'example@example.com')
