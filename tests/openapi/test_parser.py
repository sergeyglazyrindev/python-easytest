from unittest import TestCase
from easytest.openapi.parser import OpenApiNormalizer


class OpenApiNormalizerTestCase(TestCase):

    def test_subtype(self):
        config = {
            "properties": {
                "cof_ids": {
                    "description": "[subtype:integer]",
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "friend": {
                    "description": "",
                    "type": "string"
                },
                "id": {
                    "description": "",
                    "type": "integer"
                },
                "user": {
                    "description": "",
                    "type": "string"
                }
            },
        }
        self.assertEquals(
            OpenApiNormalizer.normalize(config),
            {
                "properties": {
                    "cof_ids": {
                        "description": "[subtype:integer]",
                        "items": {
                            "type": "integer"
                        },
                        "subtype": "integer",
                        "type": "array"
                    },
                    "friend": {
                        "description": "",
                        "type": "string"
                    },
                    "id": {
                        "description": "",
                        "type": "integer"
                    },
                    "user": {
                        "description": "",
                        "type": "string"
                    }
                },
            }
        )

    def test_choices(self):
        config = {
            "properties": {
                "cof_ids": {
                    "description": "[choices:qweqweqw++++21312]",
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "user": {
                    "description": "",
                    "type": "string"
                }
            },
        }
        self.assertEquals(
            OpenApiNormalizer.normalize(config),
            {
                "properties": {
                    "cof_ids": {
                        "description": "[choices:qweqweqw++++21312]",
                        "choices": ['qweqweqw', '21312'],
                        "items": {
                            "type": "string"
                        },
                        "type": "array"
                    },
                    "user": {
                        "description": "",
                        "type": "string"
                    }
                },
            }
        )

    def test_default(self):
        config = {
            "properties": {
                "cof_ids": {
                    "description": "[default:1]",
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "user": {
                    "description": "",
                    "type": "string"
                }
            },
        }
        self.assertEquals(
            OpenApiNormalizer.normalize(config),
            {
                "properties": {
                    "cof_ids": {
                        "description": "[default:1]",
                        "default": '1',
                        "items": {
                            "type": "string"
                        },
                        "type": "array"
                    },
                    "user": {
                        "description": "",
                        "type": "string"
                    }
                },
            }
        )
