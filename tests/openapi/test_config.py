from easytest.openapi.config import OpenApiConfig
from unittest import TestCase
import os
from .common import Request
from easytest.openapi.data_generators import DataGenerator


class OpenApiConfigTestCase(TestCase):

    def test(self):
        config = OpenApiConfig(os.path.join(
            os.path.dirname(__file__),
            'api.json',
        ))
        self.assertEquals(
            type(config.generate_fixture_for(
                DataGenerator(),
                '/api/core/current_user/confirm_email/',
                'post',
                Request()
            )['confirmation_code']),
            str
        )
