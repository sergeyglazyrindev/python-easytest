from easytest.config import Config
from unittest import TestCase
import easytest
import os


class ConfigTestCase(TestCase):

    def test_config(self):
        test_config = Config(
            os.path.join(os.path.dirname(__file__), 'test.xml')
        )
        test_config.parse_config()
        self.assertTrue(
            test_config.validate_config_against_dtd(
                os.path.join(os.path.dirname(
                    easytest.__file__
                ), 'dtd.xml')
            ),
            'Easy Test XML doesnt follow DTD schema'
        )
