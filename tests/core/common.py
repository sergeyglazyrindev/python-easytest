from easytest.request import BaseRequest, BaseTestVisitor


class Request(BaseRequest):

    def __init__(self):
        pass

    def force_authenticate_user(self, user):
        pass

    def check_result_after_put(self, testvisitor, resp):
        pass

    def check_result_after_list(self, testvisitor, resp):
        pass

    def check_result_after_get(self, testvisitor, resp):
        pass

    def check_result_after_delete(self, testvisitor, resp):
        pass

    def check_result_after_post(self, testvisitor, resp):
        pass

    def do_http_request(self):
        pass


class TestVisitor(BaseTestVisitor):
    pass
