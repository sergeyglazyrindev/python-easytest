import mock
from easytest.request import RequestData
from unittest import TestCase
from easytest.openapi.config import OpenApiConfig
import os
from easytest.openapi.data_generators import DataGenerator
from .common import TestVisitor, Request
from easytest.endpoint import RestEndpoint

openapi = OpenApiConfig(os.path.join(
    os.path.dirname(__file__),
    '../openapi/api.json',
))


class CustomOpenApiHandlers:

    @classmethod
    def setup(cls, *args):
        pass

    @classmethod
    def teardown(cls, *args):
        pass

    @classmethod
    def custom_matcher(cls, *args):
        pass


dt = DataGenerator()


class RequestDataTestCase(TestCase):

    def test_add(self):
        data = RequestData()
        data.add({'test': 1})
        self.assertEquals(
            data.for_request(),
            {'test': 1}
        )


class TestVisitorTestCase(TestCase):

    def get_visitor_and_request(self):
        visitor = TestVisitor()
        visitor.set_openapi(openapi)
        visitor.set_datagenerator(dt)
        request = Request()
        request.set_endpoint(RestEndpoint(
            url='/api/core/current_user/confirm_email/',
            method='post'
        ))
        return visitor, request

    def test_generate_fixture(self):
        visitor, request = self.get_visitor_and_request()
        data = visitor.generate_fixture_for(
            '/api/core/current_user/confirm_email/',
            'post', request
        )
        self.assertEquals(
            type(data['confirmation_code']),
            str
        )

    def test_execute_setup_for(self):
        visitor, request = self.get_visitor_and_request()
        with mock.patch.object(CustomOpenApiHandlers, 'setup') as mocked_method:
            openapi.register_setup_for(
                '/api/core/current_user/confirm_email/',
                CustomOpenApiHandlers.setup
            )
            visitor.execute_setup_for(
                request
            )
            mocked_method.assert_called_with(request)

    def test_execute_teardown_for(self):
        visitor, request = self.get_visitor_and_request()
        with mock.patch.object(CustomOpenApiHandlers, 'teardown') as mocked_method:
            openapi.register_teardown_for(
                '/api/core/current_user/confirm_email/',
                CustomOpenApiHandlers.teardown
            )
            resp = {}
            visitor.execute_teardown_for(
                request, resp
            )
            mocked_method.assert_called_with(request, resp)

    def test_execute_custom_matcher_for(self):
        visitor, request = self.get_visitor_and_request()
        with mock.patch.object(CustomOpenApiHandlers, 'custom_matcher') as mocked_method:
            openapi.register_custom_matcher_for(
                '/api/core/current_user/confirm_email/',
                CustomOpenApiHandlers.custom_matcher
            )
            resp = {}
            visitor.execute_custom_matcher_for(
                request, resp
            )
            mocked_method.assert_called_with(request, resp)

    def test_is_registered_custom_matcher_for(self):
        visitor, request = self.get_visitor_and_request()
        openapi.register_custom_matcher_for(
            '/api/core/current_user/confirm_email/',
            CustomOpenApiHandlers.custom_matcher
        )
        self.assertTrue(
            visitor.is_registered_custom_matcher_for(
                request
            )
        )
