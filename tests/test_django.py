from unittest import TestCase
import subprocess
import os


class DjangoTestCase(TestCase):

    def test(self):
        subprocess.call([
            'cd ' +
            os.path.abspath(
                os.path.join(os.path.dirname(__file__), 'django/project')
            ) + '&& python manage.py test',
        ], shell=True)
