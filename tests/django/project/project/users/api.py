from rest_framework.permissions import AllowAny
from .serializers import UserSerializer
from rest_framework import viewsets
from django.contrib.auth import get_user_model


class UserViewSet(viewsets.ModelViewSet):

    permission_classes = (AllowAny, )
    serializer_class = UserSerializer

    def get_queryset(self):
        return get_user_model().objects.filter(
            id=self.request.user.pk
        )
