from setuptools import setup, find_packages


def readme():
    with open('README.rst') as f:
        return f.read()


setup(
    name='easytest',
    version='0.1',
    description='Easy testing of REST, RPC stuff',
    long_description=readme(),
    classifiers=[
        'Development Status :: 0.1 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Topic :: Testing',
    ],
    url='https://github.com/sergeyglazyrindev/easytest',
    author='Sergey Glazyrin',
    author_email='sergey.glazyrin.dev@gmail.com',
    license='MIT',
    packages=find_packages('easytest'),
    include_package_data=True,
    zip_safe=False,
    test_suite='tests',
    install_requires=[
        'PyYAML==3.12', 'lxml==3.8.0',
        'injector==0.17.0', 'termcolor==1.1.0',
        'future==0.16.0'
    ],
    extras_require={
        'dev': [
            'coverage',
            'nose',
            'pytest',
            'pytest-pep8',
            'pytest-cov',
            'mock'
        ],
        'postgres': [
            'Pyrseas==0.7.3'
        ],
        'django': [
            'Django>=1.8',
            'djangorestframework>=3.5',
            'mock',
            'django-rest-swagger==2.1.2'
        ],
    },
)
