class Resource:

    name = None
    obj = None


class ResourceRegistry(object):

    def __init__(self):
        self.resources = {}

    def register_resource(self, resource_name, value_or_executable):
        self.resources[resource_name] = Resource()
        self.resources[resource_name].name = resource_name
        self.resources[resource_name].obj = value_or_executable

    def get_resource(self, resource_name):
        return self.resources[resource_name]
