from lxml import etree
from io import StringIO
from .endpoint import (
    RestEndpoint,
    RpcEndpoint
)


class Config(object):

    def __init__(self, path_to_config):
        self._config = open(path_to_config, 'rb').read()

    def parse_config(self):
        self._parsed_config = etree.XML(self._config)

    def validate_config_against_dtd(self, path_to_dtd):
        dtd_xml = open(path_to_dtd, 'r').read()
        dtd_xml = StringIO(u'{}'.format(dtd_xml))
        dtd = etree.DTD(
            dtd_xml
        )
        is_valid = dtd.validate(self._parsed_config)
        if not is_valid:
            print(dtd.error_log.filter_from_errors())
        return is_valid

    def rest_endpoints_for_framework(self, framework):
        for resource in self._parsed_config.xpath(
            '//frameworks/framework[@type="' + framework + '"]'
                '/resources/resource'
        ):
            for rest in resource.xpath('.//rest'):
                yield resource.attrib['id'], RestEndpoint(
                    **rest.attrib
                )

        for rest in self._parsed_config.xpath('//frameworks/framework[@type="' + framework + '"]/rest'):
            yield None, RestEndpoint(
                **rest.attrib
            )

    def rpc_endpoints_for_framework(self, framework):
        for rpc in self._parsed_config.xpath(
            '//frameworks/framework[@type="' + framework + '"]/rpc'
        ):
            yield RpcEndpoint(
                **rpc.attrib
            )
