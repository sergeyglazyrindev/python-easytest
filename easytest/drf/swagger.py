# -*- coding: utf-8 -*-
import json
import yaml
import inspect
import re
from rest_framework.compat import coreapi, urlparse, coreschema
from rest_framework.schemas import (
    SchemaGenerator, field_to_schema as _field_to_schema,
)
from rest_framework import serializers
from django.utils.encoding import force_text
import rest_framework


def field_to_schema(field):
    title = force_text(field.label) if field.label else ''
    description = force_text(field.help_text) if field.help_text else ''
    if isinstance(field, serializers.ListField):
        child_schema = field_to_schema(field.child)
        return coreschema.Array(
            items=child_schema,
            title=title,
            description=description
        )
    elif isinstance(field, serializers.ChoiceField):
        return coreschema.Enum(
            enum=list(field.choices.keys()),
            title=title,
            description=description
        )
    return _field_to_schema(field)


def find_pyobj(module_path):
    module_path = module_path.split('.')
    module_name, obj_path, attr_name = module_path[0], module_path[1:-1], module_path[-1]
    module = __import__(module_name)
    for tmp in obj_path:
        module = getattr(module, tmp)
    return getattr(module, attr_name)


def processdoc(doc):
    doc = inspect.cleandoc(doc)
    for param_type, param_path in re.findall(r'\:(\w+)\:\`(.*?)\`', doc):
        if param_type == 'attr':
            pyobj = find_pyobj(param_path)
            doc = doc.replace(
                ':' + param_type + ':`' + param_path + '`',
                json.dumps(pyobj)
            )
    return doc


def get_schema_for_field_from_docstring(field):
    _type = field.get('type', 'string')
    if _type == 'string' and field.get('enum'):
        return field_to_schema(
            serializers.ChoiceField(choices=field.get('enum').split(','))
        )
    if _type == 'array' and field.get('items'):
        return field_to_schema(serializers.ListField(
            child=getattr(
                serializers,
                field['items']['type'].capitalize() + 'Field'
            )()
        ))


def _str(node):
    for field in node.fields:
        print(field.name)
        if field.name == 'tag_ids':
            import pdb
            pdb.set_trace()
    return coreapi.document._str(node)


class WrappedLink(coreapi.Link):

    def __str__(self):
        return _str(self)


def get_model_from_pyobj(pyobj):
    return re.sub(r'[^\.]+\.([^\.]+)\.models', '\\1', str(pyobj.__module__)) + '_' + str(pyobj.__name__.lower())


class CustomSchemaGenerator(SchemaGenerator):
    def get_link(self, path, method, view):
        """
        __doc__ in yaml format, eg:

        desc: the desc of this api.
        parameters:
        - name: mobile
          desc: the mobile number
          type: string
          required: true
          location: query
        - name: promotion
          desc: the activity id
          type: int
          required: true
          location: form
        """
        fields = self.get_path_fields(path, method, view)
        yaml_doc = None
        func = self.get_action_from_view(view)
        if func and func.__doc__:
            try:
                yaml_doc = yaml.load(processdoc(func.__doc__))
            except:
                yaml_doc = None
        if yaml_doc and 'desc' in yaml_doc:
            desc = yaml_doc.get('desc', '')
            _method_desc = desc
            params = yaml_doc.get('parameters', [])
            for i in params:
                _name = i.get('name')
                _desc = i.get('desc')
                _required = i.get('required', True)
                _type = i.get('type', 'string')
                _location = i.get('location', 'query')
                kwargs = {}
                kwargs['schema'] = get_schema_for_field_from_docstring(i)
                if not kwargs['schema']:
                    kwargs['type'] = _type
                f = coreapi.Field(
                    name=_name,
                    location=_location,
                    required=_required,
                    description=_desc,
                    **kwargs
                )
                fields.append(f)
            params = yaml_doc.get('post_parameters', [])
            for i in params:
                _name = i.get('name')
                _required = i.get('required', True)
                _type = i.get('type', 'string')
                _location = i.get('location', 'form')
                _desc = i.get('desc')
                kwargs = {}
                kwargs['schema'] = get_schema_for_field_from_docstring(i)
                if not kwargs['schema']:
                    kwargs['type'] = _type
                field = coreapi.Field(
                    name=_name,
                    location=_location,
                    required=_required,
                    description=_desc,
                    **kwargs
                )
                fields.append(field)
        else:
            _method_desc = processdoc(
                func.__doc__
            ) if func and func.__doc__ else ''
            fields += self.get_serializer_fields(path, method, view)
        fields += self.get_pagination_fields(path, method, view)
        fields += self.get_filter_fields(path, method, view)

        if fields and any([field.location in ('form', 'body') for field in fields]):
            encoding = self.get_encoding(path, method, view)
        else:
            encoding = None

        if self.url and path.startswith('/'):
            path = path[1:]
        serializer = self.get_serializer_for_view_and_method(view, method)
        if serializer and hasattr(serializer, 'Meta'):
            model = serializer.Meta.model
            _method_desc += '[model:' + get_model_from_pyobj(model) + ']'
        return WrappedLink(
            url=urlparse.urljoin(self.url, path),
            action=method.lower(),
            encoding=encoding,
            fields=fields,
            description=_method_desc
        )

    def get_serializer_for_view_and_method(self, view, method):
        func = self.get_action_from_view(view)
        custom_serializer = None
        if func and func.__doc__:
            try:
                yaml_doc = yaml.load(func.__doc__)
                custom_serializer = yaml_doc.get('custom_serializer')
            except:
                pass
        if not custom_serializer and hasattr(view, 'get_serializer_class') and view.get_serializer_class.__doc__:
            try:
                yaml_doc = yaml.load(view.get_serializer_class.__doc__)
                custom_serializer = yaml_doc.get('custom_serializers', {}).get(
                    method.lower()
                )
            except:
                pass
        kwargs = {}
        if hasattr(view, 'get_serializer_context'):
            kwargs = {'context': view.get_serializer_context()}
        if custom_serializer:
            serializer_class = find_pyobj(custom_serializer)
            return serializer_class(**kwargs)
        if hasattr(view, 'get_serializer'):
            try:
                serializer = view.get_serializer()
                return serializer
            except Exception as e:
                print(e)
                return
        if hasattr(view, 'serializer_class'):
            return view.serializer_class(**kwargs)

    def get_action_from_view(self, view):
        return getattr(view, view.action) if getattr(
            view, 'action', None
        ) else None

    def get_serializer_fields(self, path, method, view):
        # if method not in ('PUT', 'PATCH', 'POST'):
        #     return []

        serializer = self.get_serializer_for_view_and_method(view, method)
        if not serializer:
            return []

        if isinstance(serializer, serializers.ListSerializer):
            return [
                coreapi.Field(
                    name='data',
                    location='body',
                    required=True,
                    schema=coreschema.Array()
                )
            ]

        if not isinstance(
                serializer,
                (serializers.Serializer, serializers.ModelSerializer)
        ):
            return []

        fields = []
        for field in serializer.fields.values():
            if isinstance(field, serializers.HiddenField):
                continue
            if field.read_only and method in ('PUT', 'PATCH', 'POST', 'DELETE'):
                continue

            required = field.required and method != 'PATCH'
            description = ''
            final_field = field
            params = []
            if isinstance(field, serializers.ListField):
                final_field = field.child
                field_to_subtype = {
                    serializers.IntegerField: 'integer'
                }
                if final_field.__class__ in field_to_subtype:
                    params.append(
                        'subtype:' + field_to_subtype[final_field.__class__]
                    )
            if isinstance(final_field, serializers.ChoiceField):
                params.append(
                    'choices:' + '++++'.join(map(str, final_field.choices))
                )
            if isinstance(final_field, serializers.ModelSerializer):
                model = final_field.__class__.Meta.model
                params.append(
                    'model:' + get_model_from_pyobj(model)
                )
            if final_field.default != rest_framework.fields.empty:
                params.append(
                    'default:' + str(final_field.default)
                )
            if params:
                description = '[' + '||'.join(params) + ']'
            field = coreapi.Field(
                name=field.field_name,
                location='form',
                required=required,
                schema=field_to_schema(field),
                description=description
            )
            fields.append(field)

        return fields


def additional_schema(schema):
    """
    rest framework does not provide a way to override the schema on
    a per endpoint basis.  this is a cheap way to get the fields to
    appear on the swagger web interface through the filter_backends
    """
    class SwaggerFilterBackend(object):

        def get_schema_fields(self, view):
            fields = []
            keys = ('name', 'required', 'location')
            for field in schema.values():
                kwargs = {}
                kwargs['schema'] = get_schema_for_field_from_docstring(field)
                kwargs['description'] = field.get(
                    'desc',
                    field.get('description')
                )
                if not kwargs['schema']:
                    kwargs['type'] = field['type']
                fields.append(
                    coreapi.Field(
                        **{
                            key: val for key, val
                            in field.items() if key in keys
                        },
                        **kwargs
                    )
                )
            return fields

        def filter_queryset(self, request, queryset, view):
            # do nothing
            return queryset

    def wrapper(cls):
        if hasattr(cls, 'filter_backends'):
            cls.filter_backends = list(cls.filter_backends) + [
                SwaggerFilterBackend
            ]
        else:
            cls.filter_backends = [
                SwaggerFilterBackend
            ]
        return cls

    return wrapper
