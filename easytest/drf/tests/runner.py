try:
    from django.test.runner import DiscoverRunner
except ImportError:
    from django.test.simple import DjangoTestSuiteRunner as DiscoverRunner


class EasyTestTestRunner(DiscoverRunner):

    def build_suite(self, test_labels=None, extra_tests=None, **kwargs):
        test_labels = test_labels or ['.']
        test_labels.extend([
            'easytest.drf.tests.test_config',
        ])
        extra_tests = extra_tests or []
        return super(EasyTestTestRunner, self).build_suite(
            test_labels=test_labels, extra_tests=extra_tests, **kwargs
        )
