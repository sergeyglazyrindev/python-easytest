from easytest.config import Config
from django.test import TestCase
from django.conf import settings
import easytest
import os


class ConfigTestCase(TestCase):

    def test_config(self):
        test_config = Config(settings.PATH_TO_EASYTEST_CONFIG)
        test_config.parse_config()
        self.assertTrue(
            test_config.validate_config_against_dtd(
                os.path.join(os.path.dirname(
                    easytest.__file__
                ), 'dtd.xml')
            ),
            'Easy Test XML doesnt follow DTD schema'
        )
