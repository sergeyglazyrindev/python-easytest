import json
import mock
from django.core.management.base import BaseCommand
from ...swagger import CustomSchemaGenerator
from rest_framework_swagger.renderers import OpenAPICodec
from django.conf import settings
from django.test import RequestFactory
from rest_framework.request import Request
from rest_framework.test import force_authenticate
from django.contrib.auth import get_user_model
from django.core.handlers.wsgi import WSGIHandler
from easytest.db import DatabaseDumpFactory


class Command(BaseCommand):
    help = 'Generates an api schema'

    def handle(self, *args, **options):
        print('Start schema generation')
        generator = CustomSchemaGenerator(
            title='API Schema',
        )
        factory = RequestFactory()
        request = factory.get('/api/build')
        request.user = get_user_model()
        request.user.id = 1
        force_authenticate(request, request.user)
        wsgi_handler = WSGIHandler()
        wsgi_handler.load_middleware()
        for process_view in wsgi_handler._view_middleware:
            process_view(request, None, None, None)
        request = Request(request)
        with mock.patch('rest_framework.permissions.IsAuthenticated.has_permission') as mocked_is_authenticated:
            mocked_is_authenticated.return_value = True
            schema = generator.get_schema(request=request)
        api_file_path = settings.PATH_TO_API_JSON
        open(api_file_path, 'w').write(
            str(OpenAPICodec().encode(schema).decode())
        )
        parsed = json.loads(open(api_file_path, 'r').read())
        open(api_file_path, 'w').write(
            json.dumps(parsed, indent=4, sort_keys=True)
        )
        db_dump = DatabaseDumpFactory.get(settings.EASYTEST_DBTYPE)
        db_dump.make_dump(
            settings.DATABASES['default'],
            settings.PATH_TO_DB_YAML
        )
        print('Finished schema generation')
