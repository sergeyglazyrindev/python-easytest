from django.core.exceptions import ObjectDoesNotExist
from easytest.request import BaseRequest, BaseTestVisitor
from django.test import TestCase, TransactionTestCase
from rest_framework.test import APIClient
from easytest.exceptions import NoResourceDefinedForResourceSpecificRequestException


class NonTransactionalTestCase(TestCase):

    def runTest(self):
        pass


class TransactionalTestCase(TransactionTestCase):

    def runTest(self):
        pass


class Request(BaseRequest):

    def __init__(self):
        self.client = APIClient()

    def force_authenticate_user(self, user):
        self.client.force_authenticate(user)

    def check_result_after_put(self, testvisitor, resp):
        self.matcher.assertEquals(resp.status_code, 200)
        object_from_db = self.resource.obj.__class__.objects.get(
            **{
                self.endpoint.lookup_field: getattr(
                    self.resource.obj, self.endpoint.lookup_field
                )
            }
        )
        for key, val in self.data.for_request().items():
            self.matcher.assertEquals(
                getattr(object_from_db, key),
                val,
                'Field from db "{}" doesnt equal field in response'.format(
                    key
                )
            )

    def check_result_after_list(self, testvisitor, resp):
        self.matcher.assertEquals(resp.status_code, 200)
        self.matcher.assertTrue(len(resp.data) >= 1)

    def check_result_after_get(self, testvisitor, resp):
        self.matcher.assertEquals(resp.status_code, 200)
        object_from_db = self.resource.obj.__class__.objects.get(
            **{
                self.endpoint.lookup_field: getattr(
                    self.resource.obj, self.endpoint.lookup_field
                )
            }
        )
        for key, val in resp.data.items():
            self.matcher.assertEquals(
                getattr(object_from_db, key),
                val
            )

    def check_result_after_delete(self, testvisitor, resp):
        self.matcher.assertEquals(resp.status_code, 204)
        self.matcher.assertRaises(
            ObjectDoesNotExist,
            self.resource.obj.__class__.objects.get, **{
                self.endpoint.lookup_field: getattr(
                    self.resource.obj, self.endpoint.lookup_field
                )
            }
        )

    def check_result_after_post(self, testvisitor, resp):
        self.matcher.assertEquals(resp.status_code, 201)
        if self.resource:
            self.matcher.assertTrue(
                bool(self.resource.obj.__class__.objects.get(
                    **{
                        self.endpoint.lookup_field: resp.data[
                            self.endpoint.lookup_field
                        ]
                    }
                ))
            )

    def do_http_request(self):
        if self.endpoint.transactional:
            self.testcase = TransactionalTestCase()
        else:
            self.testcase = NonTransactionalTestCase()
        self.testcase.setUpClass()
        self.testcase.client = self.client
        method = self.endpoint.method
        if method in ('list', 'post'):
            method = 'get' if method == 'list' else method
            resp = getattr(self.testcase.client, method)(
                self.endpoint.url, data=self.data.for_request()
            )
        else:
            if not self.resource:
                raise NoResourceDefinedForResourceSpecificRequestException
            data = self.data.for_request()
            resp = getattr(self.testcase.client, method)(
                self.endpoint.url.format(id=self.resource.obj.pk),
                data=data
            )
        return resp

    def cleanup(self, testvisitor, resp):
        testvisitor.execute_teardown_for(self, resp)
        self.testcase.tearDownClass()


class TestVisitor(BaseTestVisitor):
    pass
