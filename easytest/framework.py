from .config import Config
from .openapi.data_generators import DataGenerator
from .openapi.config import OpenApiConfig
from .resource_registry import ResourceRegistry


class Framework(object):

    def __init__(self, type):
        self.framework_type = type
        self._datagenerator = DataGenerator()
        self.resource_registry = ResourceRegistry()

    def load_config(self, file_path):
        self.config = Config(file_path)
        self.config.parse_config()

    def rest_endpoints(self):
        return self.config.rest_endpoints_for_framework(self.framework_type)

    def rpc_endpoints(self):
        return self.config.rpc_endpoints_for_framework(self.framework_type)

    def load_openapi_schema(self, file_path):
        self.openapi_config = OpenApiConfig(file_path)

    @property
    def datagenerator(self):
        return self._datagenerator

    def generate_fixture_for(self, *args):
        return self.openapi_config.generate_fixture_for(
            self.datagenerator, *args
        )

    @property
    def openapi(self):
        return self.openapi_config
