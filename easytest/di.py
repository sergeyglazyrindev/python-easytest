import importlib


def get_di_for_framework(framework):
    return importlib.import_module('.' + framework + '.di', __package__)


class Injector:

    def __init__(self, framework):
        self.di = get_di_for_framework(framework)

    def get_request(self):
        return self.di.Request()

    def get_test_visitor(self):
        return self.di.TestVisitor()


def get_injector_for_framework(framework):
    return Injector(framework)
