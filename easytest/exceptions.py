class EasyTestBaseException(Exception):
    pass


class ProbablyIncorrectlyUsageOfHTTPProtocolException(EasyTestBaseException):
    pass


class NoResourceDefinedForResourceSpecificRequestException(EasyTestBaseException):
    pass


class StopProcessingEndpointException(EasyTestBaseException):
    pass
