from abc import ABCMeta, abstractproperty


class BaseEndpoint:
    __metaclass__ = ABCMeta

    priority = 1

    def __init__(self, **kwargs):
        for attr_name, value in kwargs.items():
            setattr(
                self,
                attr_name,
                value
            )

    def is_executable(self):
        return hasattr(self, 'execute')

    def is_able_to_prepare_request(self):
        return hasattr(self, 'prepare_request')

    def is_able_to_analyze_response(self):
        return hasattr(self, 'analyze_response')

    @abstractproperty
    def humanized_url(self):
        raise NotImplemented

    def __eq__(self, other):

        for key, val in self.__dict__.items():
            if getattr(other, key) != val:
                return False
        return True


class UserAction(BaseEndpoint):

    name = None

    @property
    def humanized_url(self):
        return 'User action: {name}'.format(
            name=self.name
        )


class RestEndpoint(BaseEndpoint):

    url = None
    method = None
    transactional = False
    anonymous = False
    lookup_field = 'id'

    @property
    def humanized_url(self):
        return '[{url}-{method}-{transactional}-{anonymous}]'.format(
            url=self.url,
            method='http method: ' + self.method,
            transactional='transactional' if self.transactional else 'non transactional',
            anonymous='anonymous' if self.anonymous else 'non anonymous'
        )


class RpcEndpoint(BaseEndpoint):

    url = None
    transactional = False
    anonymous = False

    @property
    def humanized_url(self):
        return '[{url}-{transactional}-{anonymous}]'.format(
            url=self.url,
            transactional='transactional' if self.transactional else 'non transactional',
            anonymous='anonymous' if self.anonymous else 'non anonymous'
        )
