from io import StringIO


def humanize_error(request, error_str, exc_info):
    return "\nDuring testing {endpoint} occured error:\n Traceback and Exception: {}\n".format(
        format_traceback(exc_info),
        endpoint=request.endpoint.humanized_url,
    )


def format_traceback(exc_info):
    import traceback as _traceback
    buf = StringIO()
    _traceback.print_exception(*exc_info, file=buf)
    buf.seek(0)
    return buf.read()
