import abc
from unittest import TestCase


class RequestData:

    def __init__(self):
        self.data = {}

    def add(self, data):
        self.data.update(data)

    def for_request(self):
        return self.data


class Matcher(TestCase):

    def runTest(self):
        pass


class BaseRequest:
    __metaclass__ = abc.ABCMeta

    method = 'get'
    data = RequestData()
    matcher = Matcher()
    resource = None

    @abc.abstractmethod
    def __init__(self):
        raise NotImplemented

    def set_endpoint(self, endpoint):
        self.endpoint = endpoint

    def we_test_resource(self, resource):
        self.resource = resource

    def execute(self, testvisitor):
        self.prepare(testvisitor)
        resp = self.do_http_request()
        self.check_response_and_db_state(testvisitor, resp)
        self.cleanup(testvisitor, resp)

    def prepare(self, testvisitor):
        testvisitor.execute_setup_for(self)

    @abc.abstractmethod
    def do_http_request(self):
        raise NotImplemented

    @abc.abstractmethod
    def force_authenticate_user(self, user):
        raise NotImplemented

    def check_response_and_db_state(self, testvisitor, resp):
        if testvisitor.is_registered_custom_matcher_for(self):
            testvisitor.execute_custom_matcher_for(self, resp)
        else:
            getattr(
                self, 'check_result_after_' + self.endpoint.method
            )(
                testvisitor, resp
            )

    def cleanup(self, testvisitor, resp):
        testvisitor.execute_teardown_for(self, resp)

    @abc.abstractmethod
    def check_result_after_put(self, testvisitor, resp):
        raise NotImplemented

    @abc.abstractmethod
    def check_result_after_list(self, testvisitor, resp):
        raise NotImplemented

    @abc.abstractmethod
    def check_result_after_get(self, testvisitor, resp):
        raise NotImplemented

    @abc.abstractmethod
    def check_result_after_delete(self, testvisitor, resp):
        raise NotImplemented

    @abc.abstractmethod
    def check_result_after_post(self, testvisitor, resp):
        raise NotImplemented


class BaseTestVisitor:
    __metaclass__ = abc.ABCMeta

    def set_openapi(self, openapi):
        self.openapi = openapi

    def set_datagenerator(self, dt):
        self.dt = dt

    def generate_fixture_for(self, *args):
        return self.openapi.generate_fixture_for(self.dt, *args)

    def execute_setup_for(self, request):
        return self.openapi.execute_setup_for(request)

    def execute_teardown_for(self, request, resp):
        return self.openapi.execute_teardown_for(request, resp)

    def execute_custom_matcher_for(self, request, resp):
        return self.openapi.execute_custom_matcher_for(request, resp)

    def set_resourceregistry(self, resourceregistry):
        self.resource_registry = resourceregistry

    def is_registered_custom_matcher_for(self, request):
        return self.openapi.is_registered_custom_matcher_for(request)
