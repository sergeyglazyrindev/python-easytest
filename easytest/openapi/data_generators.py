import string
import random
from .field import Field


def generate_fake_data(type):

    if type == 'integer':
        return random.randint(1, 1000)
    elif type == 'string':
        return ''.join(
            random.choice(
                string.ascii_uppercase + string.digits
            )
            for _ in range(20)
        )


class BaseCustomDataGenerator(object):

    @classmethod
    def generate_if_needed_data_based_on_context(
            cls, field_data, request
    ):
        pass


class DataGenerator(object):

    def __init__(self):
        self.generators = []

    def register_specific_generator(self, generator_class):
        self.generators.append(generator_class)

    def generate_fake_request_data(self, openapi_schema, request):
        data = {}
        for params in openapi_schema:
            if (params.get('in') != 'body' or params.get('name') != 'data') and\
               'schema' not in params and params['schema']['type'] != 'object':
                continue
            for field, field_config in params['schema']['properties'].items():
                for generator in self.generators:
                    generated = generator.generate_if_needed_data_based_on_context(
                        Field(name=field, **field_config), request
                    )
                    if generator is not None:
                        data[field] = generated
                        break
                if field in data and data[field]:
                    continue
                if field_config.get('default'):
                    data[field] = field_config.get('default')
                    continue
                if field_config['type'] == 'integer':
                    data[field] = generate_fake_data('integer')
                elif field_config['type'] == 'boolean':
                    data[field] = random.choice([True, False])
                elif field_config['type'] == 'string':
                    if field_config.get('choices'):
                        data[field] = random.choice(
                            field_config.get('choices')
                        )
                    else:
                        data[field] = generate_fake_data('string')
                elif field_config['type'] == 'array':
                    if field_config.get('choices'):
                        data[field] = [
                            random.choice(
                                field_config.get('choices')
                            )
                            for _ in range(5)
                        ]
                    else:
                        data[field] = [
                            generate_fake_data(field_config['items']['type'])
                            for _ in range(5)
                        ]
        return data                

    def build_output_for(self, openapi_schema, data):

        def output_builder(row_of_data, schema):
            data = {}
            for field, field_config in schema['properties'].items():
                if field_config['type'] == 'integer':
                    data[field] = int(row_of_data[field])
                elif field_config['type'] == 'boolean':
                    data[field] = bool(row_of_data[field])
                elif field_config['type'] == 'string':
                    data[field] = str(row_of_data[field])
                elif field_config['type'] in ('object', ):
                    data[field] = output_builder(row_of_data[field], field_config)
                elif field_config['type'] in ('array', ):
                    new_data = []
                    for data_ in row_of_data[field]:
                        new_data.append(output_builder(data_, field_config))
                    data[field] = new_data
                else:
                    data[field] = row_of_data[field]
            return data

        if openapi_schema['type'] == 'object':
            return output_builder(data, openapi_schema)
        elif openapi_schema['type'] == 'array':
            new_data = []
            for data_ in data:
                new_data.append(output_builder(data_, openapi_schema))
            return new_data
