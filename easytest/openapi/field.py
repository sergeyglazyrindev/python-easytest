class Field(object):

    description = None
    type = None
    default = None
    choices = None
    subtype = None
    name = None

    def __init__(self, **kwargs):
        for attr_name, value in kwargs.items():
            setattr(
                self,
                attr_name,
                value
            )
