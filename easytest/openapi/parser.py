import re


class OpenApiNormalizer(object):

    @classmethod
    def normalize(cls, config):
        for key in list(config.keys()):
            val = config[key]
            if isinstance(val, dict):
                cls.normalize(val)
                continue
            if isinstance(val, list):
                for _val in val:
                    if isinstance(_val, dict):
                        cls.normalize(_val)
                    continue
                continue
            if key == 'description':
                config.update(cls._parse_extra_description(val))
        return config

    @classmethod
    def _parse_extra_description(cls, text):
        extra_params = {}
        match = re.search(r'\[(.+)\]', text)
        if match:
            match = match.group(1)
            for param in match.split('||'):
                try:
                    param_name, value = param.split(':', 1)
                except ValueError:
                    continue
                if param_name == 'default':
                    extra_params['default'] = value
                elif param_name == 'choices':
                    extra_params['choices'] = value.split('++++')
                elif param_name == 'subtype':
                    extra_params['items'] = {
                        "type": value
                    }
                    extra_params['subtype'] = value
        return extra_params
