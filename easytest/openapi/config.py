import warnings
from .data_generators import DataGenerator
import json
from ..exceptions import ProbablyIncorrectlyUsageOfHTTPProtocolException
from .parser import OpenApiNormalizer
from collections import defaultdict


def traverse_through_schema_to_deref_all_component_schemas(openapi_schema, schema_to_deref):
    dereferred_schema = schema_to_deref
    if schema_to_deref.get('$ref'):
        dereferred_schema = traverse_through_schema_to_deref_all_component_schemas(openapi_schema, openapi_schema['components']['schemas'][schema_to_deref.get('$ref').rsplit('/')[-1]].copy())
        dereferred_schema['type'] = openapi_schema['components']['schemas'][schema_to_deref.get('$ref').rsplit('/')[-1]]['type']
        return dereferred_schema
    for property_, desc in schema_to_deref['properties'].items():
        if desc.get('$ref'):
            dereferred_schema['properties'][property_] = traverse_through_schema_to_deref_all_component_schemas(openapi_schema, openapi_schema['components']['schemas'][desc.get('$ref').rsplit('/')[-1]].copy())
    return dereferred_schema


class OpenApiConfig(object):

    def __init__(self, path_to_file):
        self.path_to_file = path_to_file
        self.config = json.loads(
            open(path_to_file, 'r').read()
        )
        OpenApiNormalizer.normalize(self.config)
        self.teardown_handlers = defaultdict(dict)
        self.setup_handlers = defaultdict(dict)
        self.custom_matchers = defaultdict(dict)

    def generate_fixture_for(self, dt, url, method, request):
        if method.lower() in ('get', 'list'):
            warnings.warn(
                'You cant pass any data in GET requests '
                'cause its unsecure. You asked to generate '
                'fixture for url: {}'.format(
                    url
                ),
                UserWarning
            )
        return dt.generate_fake_request_data(
            [
                params
                for params in self.config['paths'][url][method]['parameters']
                if 'schema' in params
            ],
            request
        )

    def build_output_for(self, dt, data, url, method, resp_code, content_type='application/json'):
        expected_output_schema = self.config['paths'][url][method]['responses'][str(resp_code)]['content'][content_type]['schema'].copy()
        expected_output_schema = traverse_through_schema_to_deref_all_component_schemas(self.config, expected_output_schema)
        return dt.build_output_for(
            expected_output_schema,
            data
        )

    def is_url_present_in_schema(self, url, method, resp_code, content_type='application/json'):
        return bool(self.config['paths'].get(url, {}).get(method, {}).get('responses', {}).get(str(resp_code), {}).get('content', {}).get(content_type, {}).get('schema', {}))

    def register_teardown_for(self, url, teardown,  method='all'):
        self.teardown_handlers[url][method] = teardown

    def register_setup_for(self, url, setup,  method='all'):
        self.setup_handlers[url][method] = setup

    def register_custom_matcher_for(self, url, assert_func,  method='all'):
        self.custom_matchers[url][method] = assert_func

    def execute_setup_for(self, request):
        if request.endpoint.url not in self.setup_handlers:
            return
        url = request.endpoint.url
        method = request.endpoint.method
        if method not in self.setup_handlers[url]:
            if 'all' not in self.setup_handlers[url]:
                return
            method = 'all'
        self.setup_handlers[url][
            method
        ](request)

    def execute_teardown_for(self, request, resp):
        if request.endpoint.url not in self.teardown_handlers:
            return
        url = request.endpoint.url
        method = request.endpoint.method
        if method not in self.teardown_handlers[url]:
            if 'all' not in self.teardown_handlers[url]:
                return
            method = 'all'
        self.teardown_handlers[url][
            method
        ](request, resp)

    def execute_custom_matcher_for(self, request, resp):
        if request.endpoint.url not in self.custom_matchers:
            return
        url = request.endpoint.url
        method = request.endpoint.method
        if method not in self.custom_matchers[url]:
            if 'all' not in self.custom_matchers[url]:
                return
            method = 'all'
        self.custom_matchers[url][
            method
        ](request, resp)

    def is_registered_custom_matcher_for(self, request):
        if request.endpoint.url not in self.custom_matchers:
            return False
        url = request.endpoint.url
        method = request.endpoint.method
        if method not in self.custom_matchers[url]:
            if 'all' not in self.custom_matchers[url]:
                return False
        return True
