import subprocess


class PostgresDump(object):

    @classmethod
    def make_dump(cls, db_settings, path):
        subprocess.check_call([
            'dbtoyaml -H ' + db_settings['HOST'] + ' -U ' +
            db_settings['USER'] + ' ' + db_settings['NAME'] + ' > ' + path
        ], shell=True)


class NoRegisteredDbDump:

    @classmethod
    def make_dump(cls, db_settings, path):
        pass


class DatabaseDumpFactory(object):

    @classmethod
    def get(cls, db_type):
        db_type_to_class = {
            'postgres': PostgresDump
        }
        try:
            return db_type_to_class[db_type]
        except KeyError:
            return NoRegisteredDbDump
